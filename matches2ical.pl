#! /usr/bin/perl
#
# This program is licenced under the
# GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
#
# Copyright 2022 by Christian Sperr
# ----------------------------------------------------
#
# input: Download of type: "Begegnungen(Filter Meisterschaft und Status)"
# output: An iCal file for each team with all matches and one file with the matches of all teams.
#         The files are written to a directoy icals, wich must exist before running the script.
#

use strict;
use warnings;
use open qw(:std :utf8); # open std input/output channels as utf-8
use utf8;

my %Teams = ();
my @AllEvents = ();
my $CRLF = "\r\n";

while (<>) {

    my @fields = split(";");

    next unless $fields[0] eq "STT";

    my $liga = $fields[5];
    my $runde = $fields[6];
    my $termin = $fields[8];
    my $lokal = $fields[10];
    my $heimclub = $fields[13];
    my $kategorie = $fields[14];
    my $heimnr = $fields[15];
    my $gastclub = $fields[18];
    my $gastnr = $fields[20];

    next if $heimclub eq "spielfrei";
    next if $gastclub eq "spielfrei";

    my $hometeam = "$heimclub $kategorie $heimnr";
    my $gastteam = "$gastclub $kategorie $gastnr";
    my @event = ($hometeam, $gastteam, $termin, $lokal, $liga, $runde, $heimclub);

    if (! exists($Teams{$hometeam})) {
        $Teams{$hometeam} = [];
    }
    if (! exists($Teams{$gastteam})) {
        $Teams{$gastteam} = [];
    }
    push(@{$Teams{$hometeam}}, (\@event));
    push(@{$Teams{$gastteam}}, (\@event));
    push(@AllEvents, (\@event));

}

$Teams{"Alle"} = \@AllEvents;

foreach my $team (sort keys(%Teams)) {
    print("working on $team\n");

    my $filename = $team;
    $filename =~ s/ /_/g;
    $filename .= ".ics";

    open(OUTF , ">icals/$filename") || die "coud not open $filename\n";

    print(OUTF "BEGIN:VCALENDAR" . $CRLF);
    print(OUTF "VERSION:2.0" . $CRLF);
    print(OUTF "PRODID:-//csv2ical//perl script//EN" . $CRLF);


    foreach my $event (@{$Teams{$team}}) {

        print(OUTF makeVEvent($event));

    }

    print(OUTF "END:VCALENDAR" . $CRLF);

    close(OUTF);

}


sub makeVEvent {

    my @event = @{shift()};

    my $vevent = "BEGIN:VEVENT" . $CRLF
        . "DTSTART:" . getStart($event[2]) . $CRLF
        . "DTEND:" . getEnd($event[2]) . $CRLF
        . "SUMMARY:" . "$event[0] - $event[1]" . $CRLF
        . "DESCRIPTION:" . "$event[4]; $event[5]; $event[3]" . $CRLF
        . "LOCATION:" . "$event[6]" . $CRLF
        . "TRANSP:OPAQUE" . $CRLF
        . "CLASS:PUBLIC" . $CRLF
        . "END:VEVENT" . $CRLF;


    return $vevent;

}

sub getStart {
    my $termin = shift();
    my ($d, $m, $y, $hour, $minute) = $termin =~ /(\d\d).(\d\d).(\d\d\d\d) (\d\d):(\d\d)/;
    return "$y$m$d" . "T" . "$hour$minute" . "00";
}

sub getEnd {
    my $termin = shift();
    my ($d, $m, $y, $hour, $minute) = $termin =~ /(\d\d).(\d\d).(\d\d\d\d) (\d\d):(\d\d)/;
    $hour += 2;
    $hour = substr("00$hour", -2, 2);
    return "$y$m$d" . "T" . "$hour$minute" . "00";
}
