#! /usr/bin/perl
#
# This program is licenced under the
# GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
#
# Copyright 2022 by Christian Sperr
# ----------------------------------------------------
# read club data downloaded from nuliga (aka click-tt.ch)
# and write a LaTeX command with the address of each club
# to the standard output.
#
# i.e one line like the following for each club
# \newcommand{\KACity}{TTC City\\Administrator Name\\Club Address\\zip \textbf{city}}
#

use strict;
use warnings;
use POSIX qw(ceil);
use open qw(:std :utf8); # open std input/output channels as utf-8

while (<>) {

    my @fields = split(";");

    next unless $fields[0] eq "STT";
    next if $fields[5] eq "TTVI";

    my $VereinName = "Tischtennisclub $fields[5]";
    my $AdresseName = "\\\\$fields[21]";
    my $AdresseStrasse = "\\\\$fields[22]";
    my $AdressePLZ = "\\\\$fields[23]";
    my $AdresseOrt = $fields[24];

    $AdresseName = $AdresseName eq "\\\\$VereinName" ? "" : $AdresseName;
    $AdresseName = $AdresseName eq "\\\\-" ? "" : $AdresseName;
    $AdresseStrasse = $AdresseStrasse eq "\\\\-" ? "" : $AdresseStrasse;
    my $command = $fields[5];
    $command =~ s/[ äöü-]//g;

    print("\\newcommand{\\KA$command}{$VereinName$AdresseName$AdresseStrasse"
          . "$AdressePLZ \\textbf{$AdresseOrt}}\n");
}

