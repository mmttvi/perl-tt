#! /usr/bin/perl
#
#
# This program is licenced under the
# GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
#
# Copyright 2022 by Christian Sperr
# ----------------------------------------------------
#
# input: Download of type: "Begegnungen(Filter Meisterschaft und Status)"
# output: a statistic concerning matches open and already played.
#

use strict;
use warnings;
use Time::Local;
use POSIX qw(strftime);
use open qw(:std :utf8); # open std input/output channels as utf-8

my $statistics = {};

while (<>) {

    my @fields = split(";");

    next unless $fields[0] eq "STT";

    my $altersklasse = $fields[3];
    my $status = $fields[25];

    if (!exists $statistics->{$altersklasse}) {
        $statistics->{$altersklasse} = [];
        @{$statistics->{$altersklasse}}[0] = 0;
        @{$statistics->{$altersklasse}}[1] = 0;
        @{$statistics->{$altersklasse}}[2] = 0;
    }
    @{$statistics->{$altersklasse}}[0]++;
    if ($status eq "abgeschlossen") {
        @{$statistics->{$altersklasse}}[1]++;
    } else {
        @{$statistics->{$altersklasse}}[2]++;
    }
}

foreach my $key (sort keys %{$statistics}) {
    my $quot =100.0 * @{$statistics->{$key}}[1] / @{$statistics->{$key}}[0];
    print "$key  $statistics->{$key}[0] $statistics->{$key}[1] $statistics->{$key}[2] : $quot%\n"
};
