#! /usr/bin/perl
#
# This program is licenced under the
# GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
#
# Copyright 2022 by Christian Sperr
# ----------------------------------------------------
#
# input: Download of type: "Begegnungen(Filter Meisterschaft und Status)"
# output: a line for each club and date, where a match shall be played.
#         The line contains the club,  the date, the day of the week,
#         a count of games at home, a count of
#         games not at home, a total count of matches on that date and a list of
#         all teams that have a match with an indication if the team plays at home.
#

use strict;
use warnings;
use Time::Local;
use POSIX qw(strftime);
use open qw(:std :utf8); # open std input/output channels as utf-8
use utf8;

my %Clubs = ();

while (<>) {
    my @fields = split(";");

    next unless $fields[0] eq "STT";

    my $termin = $fields[8];
    my $heimclub = $fields[13];
    my $kategorie = $fields[14];
    my $heimnr = $fields[15];
    my $gastclub = $fields[18];
    my $gastnr = $fields[20];

    my ($date, $time) =  $termin =~ /((?:\d\d)\.(?:\d\d)\.(?:\d\d\d\d)) ((?:\d\d):(?:\d\d))/;
    $date =~ s/(\d\d)\.(\d\d)\.(\d\d\d\d)/$3-$2-$1/;

    if (! exists $Clubs{$heimclub} ) {
        $Clubs{$heimclub} = {};
    }
    if (! exists $Clubs{$heimclub}->{$date}) {
        $Clubs{$heimclub}->{$date} = {};
        $Clubs{$heimclub}->{$date}->{"teams"} = [];
        $Clubs{$heimclub}->{$date}->{"homecount"} = 0;
        $Clubs{$heimclub}->{$date}->{"guestcount"} = 0;
    }

    push(@{$Clubs{$heimclub}->{$date}->{"teams"}}, "$kategorie->$heimnr (H)");
    $Clubs{$heimclub}->{$date}->{"homecount"}++;


    if (! exists $Clubs{$heimclub} ) {
        $Clubs{$gastclub} = {};
    }
    if (! exists $Clubs{$gastclub}->{$date}) {
        $Clubs{$gastclub}->{$date} = {};
        $Clubs{$gastclub}->{$date}->{"teams"} = [];
        $Clubs{$gastclub}->{$date}->{"homecount"} = 0;
        $Clubs{$gastclub}->{$date}->{"guestcount"} = 0;
    }

    push(@{$Clubs{$gastclub}->{$date}->{"teams"}}, "$kategorie->$gastnr (A)");
    $Clubs{$gastclub}->{$date}->{"guestcount"}++;

}

foreach my $club (sort keys(%Clubs)) {
    foreach my $playday (sort keys(%{$Clubs{$club}})) {
        my $teamlist = join(", ", @{$Clubs{$club}->{$playday}->{"teams"}});
        my $teamsplaying = scalar(@{$Clubs{$club}->{$playday}->{"teams"}});
        my $homecount = $Clubs{$club}->{$playday}->{"homecount"};
        my $guestcount = $Clubs{$club}->{$playday}->{"guestcount"};
        my $weekday = weekday($playday);
        print("$club;$playday;$weekday;$homecount;$guestcount;$teamsplaying;$teamlist \n");
    }
}

sub weekday() {

    my $date = shift;

    my ($year, $month, $day) = split '-', $date;
    my $epoch = timelocal( 0, 0, 0, $day, $month - 1, $year - 1900 );
    strftime( "%a", localtime( $epoch ) );

}
