#! /usr/bin/perl
#
# This program is licenced under the
# GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
#
# Copyright 2022 by Christian Sperr
# ----------------------------------------------------
#
# input: Download of type: "Stammpielermeldung kompakt als csv" for all teames of a ligue
#         for correct encoding input must be utf8.
# output: all teams of the league with the sum of the classificaction point for the best
#         three players of the team
#
# Christian Sperr, 12.07.2019
# =======================================================================

use strict;
use warnings;
use open qw(:std :utf8); # open std input/output channels as utf-8
use utf8;

my %data = ();
while (<>) {

    next if m/^MeisterschaftName.*/;

    my @inputline = split(";");
    my $mannschaft = $inputline[4];
    my $spielerrang = $inputline[7];
    my $klassierung = $inputline[8];
    $klassierung =~ s/^[A-D]//;

    next if ($spielerrang > 3);

    my $key = $mannschaft;

    print STDERR "$key  -> $spielerrang $klassierung\n";

    if (! exists($data{$key})) {
       $data{$key} = 0 + $klassierung;
    } else {
        $data{$key} += $klassierung;
    }
}

my @teams;

foreach my $team (keys(%data)) {
    push(@teams, (sprintf("%3d - %s", $data{$team},  $team)));
}

my @sortedteams = reverse(sort(@teams));

foreach my $output (@sortedteams) {
    print("$output\n");
}
