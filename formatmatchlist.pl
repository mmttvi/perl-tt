#! /usr/bin/perl
#
# This program is licenced under the
# GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
#
# Copyright 2022 by Christian Sperr
# ----------------------------------------------------
#
# input: Download of type: "Begegnungen(Filter Meisterschaft und Status)"
# output: a csv file with the most important fields of the input file
#
# options:
#   --home-filter -h : limits output to home matches of the given club

use strict;
use warnings;
use Getopt::Long;
use open qw(:std :utf8); # open std input/output channels as utf-8
use Encode qw(decode_utf8);
use utf8;
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $homefilter;

GetOptions ('home-filter|h=s' => \$homefilter);

while (<>) {

    my @fields = split(";");

    next unless $fields[0] eq "STT";

    my $liga = $fields[5];
    my $runde = $fields[6];
    my $termin = $fields[8];
    my $lokal = $fields[10];
    my $heimclub = $fields[13];
    my $heimnr = $fields[15];
    my $gastclub = $fields[18];
    my $gastnr = $fields[20];

    my ($date, $time) =  $termin =~ /((?:\d\d)\.(?:\d\d)\.(?:\d\d\d\d)) ((?:\d\d):(?:\d\d))/;

    $date =~ s/(\d\d)\.(\d\d)\.(\d\d\d\d)/$3-$2-$1/;

    if (!$homefilter || ($homefilter eq $heimclub)) {
        print("$liga;$runde;$date;$time;$lokal;$heimclub;$heimnr;$gastclub;$gastnr\n");
    }
}

