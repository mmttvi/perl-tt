# perl-tt

Some perl scripts helpful in creating a plan for a championship
season.

All scripts should read and write UTF-8

## Perl Scripts to convert csv-files downloaded from nuliga-admin

### clubs2addresses.pl

This script takes a file created with the download

**Vereine(Stammdaten, Adressen und Spiellokale)**

and creates output in LaTeX, that defines a command for the address of
each Club

### matches2protocolformcount.pl

This script reads a file created by the download

**Begegnungen(Filter Meisterschaft und Status)**

and writes a table with the count of match protocol forms eaqch club
will need to STDOUT.  The count is increased by at least 10% to allow
some errors in filling out the forms.

### testmatchlist.pl

This script reads a file created by the download

**Begegnungen(Filter Meisterschaft und Status)**

and checks it for matches on "Allerheiligen" and "Mariä Empfängnis",
teams having more than one match on the same date and teams having
more than one match in the same calendar week.


### matchliststatistics.pl

This script reads a file created by the download

**Begegnungen(Filter Meisterschaft und Status)**

and outputs a small statistic of number and percentage of matches
already played. This was used to detect if the season woul count when
interrupted through covid measures.

### matches2ical.pl

This script reads a file created by the download

**Begegnungen(Filter Meisterschaft und Status)**

and outputs iCal files containing calendar entries for each teams matches and
an additional file containing calendar entries for all matches. The files are written 
to a directory 'icals' under the current working directory. This subdirecrtory must
exist prior to running the script.

### formatmatchlist.pl

This script reads a file created by the download

**Begegnungen(Filter Meisterschaft und Status)**

and outputs a new csv file with the most important fields. If the option '--home-filter'
is set to a name of a club. Only matches played at this clubs
home site are output.

### matches2playdays.pl

This script reads a file created by the download

**Begegnungen(Filter Meisterschaft und Status)**

and outputs a line for each club and date, where a match shall be played.
The line contains the club,  the date, the day of the week,
a count of games at home, a count of
games not at home, a total count of matches on that date and a list of
all teams that have a match with an indication if the team plays at home.

### grouphelper.pl

This script reads a file created by downloading the 

**Stammpielermeldung kompakt als csv**

from a group containing all teh teams of league, converted to utf-8. 
and outputs a list of all the teams sorted by descending sum of
classification point assigned to the three best players of the team.

### stammspielerlist.pl


This script reads a file created by downloading the

**Stammpielermeldung kompakt als csv**

and concatenating them for all groups of all leagues and outputs a list of all 
teams with their principal players. Additionally it checks for players
that are memeber of multiple teams.

