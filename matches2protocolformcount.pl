#! /usr/bin/perl
#
# This program is licenced under the
# GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
#
# Copyright 2022 by Christian Sperr
# ----------------------------------------------------
#
# input: Download of type: "Begegnungen(Filter Meisterschaft und Status)"
# output: a table with a count of needed match protocol forms for each club
#  the count is increased by 10% to allow for some errors in filling out
#  the form.
#

use strict;
use warnings;
use POSIX qw(ceil);
use open qw(:std :utf8); # open std input/output channels as utf-8
use utf8; # allow utf 8 in source-code

my %Clubs = ();
my $gameCount;
my $club;
my $reserve;
my $total;
my $grandTotal = 0;


while (<>) {

    my @fields = split(";");

    next unless $fields[0] eq "STT";
    next if $fields[3] =~ /Nachwuchs.*/;

    my $heimclub = $fields[13];

    if (!exists $Clubs{$heimclub}) {
        $Clubs{$heimclub} = {};
        $Clubs{$heimclub}->{"count"} = 0;
    }

    $Clubs{$heimclub}->{"count"}++;
}

foreach  $club (sort keys(%Clubs)) {
    $gameCount = $Clubs{$club}->{"count"};
    $reserve = ceil($gameCount / 10);
    $total = $gameCount + $reserve;
    $grandTotal += $total;
    write;
}

print("\nTotal : $grandTotal\n");

format STDOUT_TOP=
Matchblätter pro Klub

                                          Total
Klub                 Heimspiele  Reserve  Matchblätter
------------------------------------------------------
.

format STDOUT=
@<<<<<<<<<<<<<<<<<<  @>>>>>>>>>   @>>>>>  @>>>>>>>>>>>
$club, $gameCount, $reserve, $total
.

