#! /usr/bin/perl
#
# This program is licenced under the
# GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
#
# Copyright 2022 by Christian Sperr
# ----------------------------------------------------
#
# input: Concatenated downloads of type: "Stammpielermeldung kompakt als csv" for all teams of all leagues
#        for correct encoding input must be utf8.
# output: A list of all teams with their principal players.
#         Additionally it is checked, which players are members of more than one team.
#
# Christian Sperr, 14.07.2021
# =======================================================================

use strict;
use warnings;
use open qw(:std :utf8); # open std input/output channels as utf-8
use utf8;

my %teams = ();
my %players = ();


while (<>) {

    next if m/^MeisterschaftName.*/;

    my @inputline = split(";");
    my $staffel = $inputline[2];
    my $mannschaft = $inputline[4];
    my $spielerrang = $inputline[7];
    my $klassierung = $inputline[8];
    my $playerName = $inputline[9];

    $klassierung =~ s/^[A-D]//;

    next if ($spielerrang > 3);

    $mannschaft = "$staffel->$mannschaft";

    if (! exists($teams{$mannschaft})) {
       $teams{$mannschaft} = [];
    }
    push(@{$teams{$mannschaft}}, $playerName);

    if (! exists($players{$playerName})) {
       $players{$playerName} = [];
    }
    push(@{$players{$playerName}}, $mannschaft);
}

foreach my $team (sort(keys(%teams))) {
    my $members = join("; ", @{$teams{$team}});
    print("$team: $members\n");
}

print("\n\nPlayers in more than one Team\n\n");

my %crosslinkedTeams = ();

foreach my $player (sort(keys(%players))) {

    if (scalar(@{$players{$player}}) > 1) {
        my $members = join(", ", sort(@{$players{$player}}));
        my $clteams = join("<->", sort(@{$players{$player}}));
        $crosslinkedTeams{$clteams} = 1;
        print("$player: $members\n");
    }
}

print("\n\nCrosslinked Teams\n\n");

foreach my $clteams (sort(keys(%crosslinkedTeams))) {
    print("$clteams\n");
}
