#! /usr/bin/perl
#
# This program is licenced under the
# GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or later
#
# Copyright 2022 by Christian Sperr
# ----------------------------------------------------
#
# input: Download of type: "Begegnungen(Filter Meisterschaft und Status)"
# output: tests some aspects of the read match list. I.e.:
# - list groups with matches on
#   - 01.11. Allerheiligen
#   - 08.12. Mariä Empfängnis
# - teams with more than one match
#   - on the same day
#   - in the same calendar week

use strict;
use warnings;
use Time::Local;
use POSIX qw(strftime);
use open qw(:std :utf8); # open std input/output channels as utf-8
use utf8;

my %Clubs = ();


while (<>) {

    my @fields = split(";");

    next unless $fields[0] eq "STT";

    my $liga = $fields[5];
    my $termin = $fields[8];
    my $heimclub = $fields[13];
    my $kategorie = $fields[14];
    my $heimnr = $fields[15];
    my $gastclub = $fields[18];
    my $gastnr = $fields[20];

    my $heimteam = "$heimclub $kategorie $heimnr";
    my $gastteam = "$gastclub $kategorie $gastnr";

    my ($date, $time) =  $termin =~ /((?:\d\d)\.(?:\d\d)\.(?:\d\d\d\d)) ((?:\d\d):(?:\d\d))/;

    my ($day, $month, $year) = split '\\.', $date;

    my $epoch = timelocal( 0, 0, 0, $day, $month - 1, $year - 1900 );
    my $week  = strftime( "%U", localtime( $epoch ) );

    $date =~ s/(\d\d)\.(\d\d)\.(\d\d\d\d)/$3-$2-$1/;

    if (! exists $Clubs{$heimclub} ) {
        $Clubs{$heimclub} = {};
    }
    if (! exists $Clubs{$heimclub}->{$heimteam} ) {
        $Clubs{$heimclub}->{$heimteam} = {};
        $Clubs{$heimclub}->{$heimteam}->{'week'} = {};
        $Clubs{$heimclub}->{$heimteam}->{'date'} = {};
        $Clubs{$heimclub}->{$heimteam}->{'liga'} = $liga;
    }

    if (! exists $Clubs{$heimclub}->{$heimteam}->{'week'}->{$week}) {
        $Clubs{$heimclub}->{$heimteam}->{'week'}->{$week} = [];
    }
    if (! exists $Clubs{$heimclub}->{$heimteam}->{'date'}->{$date}) {
        $Clubs{$heimclub}->{$heimteam}->{'date'}->{$date} = [];
    }

    push(@{$Clubs{$heimclub}->{$heimteam}->{"week"}->{$week}}, ("$date: $gastteam"));
    push(@{$Clubs{$heimclub}->{$heimteam}->{"date"}->{$date}}, ("$date: $gastteam"));

    if (! exists $Clubs{$gastclub} ) {
        $Clubs{$gastclub} = {};
    }
    if (! exists $Clubs{$gastclub}->{$gastteam} ) {
        $Clubs{$gastclub}->{$gastteam} = {};
        $Clubs{$gastclub}->{$gastteam}->{'week'} = {};
        $Clubs{$gastclub}->{$gastteam}->{'date'} = {};
        $Clubs{$gastclub}->{$gastteam}->{'liga'} = $liga;
    }

    if (! exists $Clubs{$gastclub}->{$gastteam}->{'week'}->{$week}) {
        $Clubs{$gastclub}->{$gastteam}->{'week'}->{$week} = [];
    }
    if (! exists $Clubs{$gastclub}->{$gastteam}->{'date'}->{$date}) {
        $Clubs{$gastclub}->{$gastteam}->{'date'}->{$date} = [];
    }

    push(@{$Clubs{$gastclub}->{$gastteam}->{"week"}->{$week}}, ("$date: $heimteam"));
    push(@{$Clubs{$gastclub}->{$gastteam}->{"date"}->{$date}}, ("$date: $heimteam"));


    if ($day == 8 && $month == 12) {
        print("Mariä Empfängnis: $liga\n");
    }

    if ($day == 1 && $month == 11) {
        print("Allerheiligen: $liga\n");
    }

}

foreach my $club (sort keys(%Clubs)) {
    foreach my $team (sort keys(%{$Clubs{$club}})) {
        foreach my $wk (sort keys(%{$Clubs{$club}->{$team}->{'week'}})) {
            if (scalar(@{$Clubs{$club}->{$team}->{'week'}->{$wk}}) >= 2) {
                print("Woche $wk: $Clubs{$club}->{$team}->{'liga'}  $team:");
                my $playlist = join(', ', @{$Clubs{$club}->{$team}->{'week'}->{$wk}});
                print("$playlist\n");
            }
        }
        foreach my $pd (sort keys(%{$Clubs{$club}->{$team}->{'date'}})) {
            if (scalar(@{$Clubs{$club}->{$team}->{'date'}->{$pd}}) >= 2) {
                print("$Clubs{$club}->{$team}->{'liga'} Tag $pd: $team:");
                my $playlist = join(', ', @{$Clubs{$club}->{$team}->{'date'}->{$pd}});
                print("$playlist\n");
            }
        }
    }
}
